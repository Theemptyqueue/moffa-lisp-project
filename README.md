John David Moffa
This README is also a change-log for what features have been added since the tart of the project.

LISP project iteration 2:

Added function coin-flipper that takes in no input parameters.
coin-flipper determines if the user or computer makes the first move in a game.
coin-flipper returns a pseudo-random integer value between zero and one-hundred.
If the integer value is greater than 50, the user gets to make the first move.
If the integer value is less than or equal to 50, the computer makes the first move.

LISP project iteration 1:

Uses a string-list to hold the board.

prints out data about the game as it's being played.
reports a win if the computer won or if the user has won.

The user, represented as X by default;
uses the keypad to the right of the keyboard,
or the number keys located under the function keys; to place an X on the board
at any location not already taken by the computer or a previous move.

Valid input values for the user are:
1, 2, 3, 4, 5, 6, 7, 8, 9

If the user enters 0 at any point, the game is ended and the program terminates.

The computer, represented as O by default; 
uses a pseudo-random number generator to generate a number 
between one and nine to place an O on the board
at any location not already taken by the user or a previous move.

When the user makes a move taking a location on the board, 
the computer cannot make that same move.  The same goes for the reverse;
If the computer makes a move taking a location on the board, 
the user can no-longer make that same move.

How the program runs in the IDE:

When the user starts this program, the following will happen:
A blank board will appear and be displayed

|-|-|-|
|-|-|-|
|-|-|-|

The blank board has dashes to indicate valid positions to take.
Then the following message is displayed to the user:
"Enter a number between 1 and 9. (entering 0 will end the program)"
Then a reference board showing the positions numerically to the user is displayed.

|1|2|3|
|4|5|6|
|7|8|9|

This is the reference board for the user that is displayed after every move.

As the game progresses, previous moves are shown to the user by displaying each
iteration of the board separately to act a guess history.

How a victory is reported:

A victory is reported to the user when there are:
Three Xs in a row horizontally 
Three Xs in a row vertically
Three Xs in a row diagonally
Three Os in a row horizontally 
Three Os in a row vertically
Three Os in a row diagonally

As an example of game-play, the following is a diagonal win for X (the user):

Remember that the board is indexed like this:

|1|2|3|
|4|5|6|
|7|8|9|

The game starts out with an empty board:

|-|-|-|
|-|-|-|
|-|-|-|

The user makes the first move. The computer follows with its move:

|X|-|-|
|-|-|-|
|O|-|-|

|X|O|-|
|-|X|-|
|O|-|-|

Diagonal Victory For X

|X|O|-|
|-|X|O|
|O|-|X|

As you can see, the board is updated 
with both the user and the computer moves recorded in the same instance

This is an example of a vertical win for O (the computer):

Remember that the board is indexed like this:

|1|2|3|
|4|5|6|
|7|8|9|

Again, the game starts out with an empty board:

|-|-|-|
|-|-|-|
|-|-|-|

|-|-|X|
|O|-|-|
|-|-|-|

|-|X|X|
|O|-|-|
|O|-|-|

Left Column Win for O

|O|X|X|
|O|X|-|
|O|-|-|