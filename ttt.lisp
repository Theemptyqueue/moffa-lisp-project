;;John David Moffa
;;CSC 450
;;ttt.lisp
;;date created: 16 November 2018
;;current edit date: 9 December 2018
;;This is a TIC-TAC-TOE game written in LISP
;; global variable for the default board before any moves are made.
;; using hyphens to note where no moves have been made.
(defparameter *startingBoard* (list "-" "-" "-" "-" "-" "-" "-" "-" "-"))

;;global variables that are used as local variables within functions.
;;So these variables are locally-global variables that are used in several functions.
;;these variables are defined globally since LISP does not recognize the 
;;defvar function for local variables 
(defvar *moveResult*)
(defvar *workingList*)
(defvar myIterator)
(defvar i)
(defvar i2)
(defvar j)
(defvar *humanIndex*)
(defvar *nthValue*)
(defvar *val*)
(defvar *newval*)
(defvar *listIndex*)
(defvar *aiListIndex*)
(defvar myBool)
(defvar myComputer)

(defvar hasWon)
(defvar whoWon)
(defvar winExplain)

;;Creating tic-tac-toe map variables.
;;local variables for positions of the empty board
(defvar v0)
(defvar v1)
(defvar v2)
(defvar v3)
(defvar v4)
(defvar v5)
(defvar v6)
(defvar v7)
(defvar v8)

;; extra variables needed to keep the program from bugging out.
(defvar cCheck)
(defvar k)
(defvar slotNumber)
(defvar myMark )
(defvar computerMark)

;;Setting the user to X and the computer to O.
(setq myMark "X" )
(setq computerMark "O")
	
;;load = (load "~/foo.lisp") || (load "foo.lisp") if in the default folder
;;prints out a tic-tac-toe board
;;params are board containing a ttt board in row-major order
(defun print-board (myBoard)
	
	;;print a tic-tac-toe board using the list provided
	(do ((i 0 (+ i 1)) )
		((= i 9) 'done)
		;;
		(if (= (mod i 3) 0)
		(format t "~%|")
		nil)
		;;print out each element of the board in an organized fashion.
		(format t "~A|" (nth i myBoard))	
	)
)

;;Reverses the element order of any list entered
(defun listreverse (mylist) 
	;;that move through the list and reverse the order of the elements in said list 
	 
	;;reversing the order of the list.
	;;myIterator is the iterator for the dolist function that iterates through the list *workingList*
	(dolist (myIterator mylist)
		(push myIterator *workingList*)
	)
	;;sending back the reversed list
	(return-from listreverse *workingList*)
)

<<<<<<< HEAD
;;Replaces the ith position in the list with value
=======
;;Replaces all dash characters on the board with Xs to represent the user
>>>>>>> b7db9fad5052c170780b1c67a82fb07c5d59d3d7
(defun my-replace (i value) 
	;;i is the index offset for the human for input to start at 1
	;;the index is then offset 
	(setq i ( - i 1))
	;;*workingList* is the working list that is being manipulated for this routine
	(setq *workingList* nil)
	;;j is the pointer to the nth position in the list.
	(setq j 0)
	;;*nthValue* is the value at the nth position of the list
	(dolist (*nthValue* *startingBoard*)
		;;if i = j, push value (X or O) onto that location of the board
		(if (= i j)
			;;pushing value into *workingList* replace what was there in the list.
			(push value *workingList*)	()
		)
		;;if pushing *nthValue* onto *workingList* keeps the values that were unchanged
		(if (/= i j)
			;;keep all the positions from *startingBoard* that haven't changed in *workingList*
			(push *nthValue* *workingList*)	()
		)
		;;move to the next position in the list.
		( setq j (+ j 1))
	)
	;;calling list reverse on *workingList* because when pushing *workingList* onto *nthValue* reversed the list.
	(listreverse *workingList*)
	;;set the starting board equal to the user input so that there is no gaps in the game grid
	(setq *startingBoard* *workingList*)
)

;;the computer makes a pseudo-random move.
(defun make-move (board)
	(do ((j 0 (+ j 1)) )
		((> j 99) 'done)  ;; make 99 attempts to find an open space
			;;these are the human number, not the index numbers.
			;;first thing to do is to check if any rows, columns, or diagonals are empty.
			;;this function adjusts based on human numbers.
			(setq *humanIndex* ( + 1 (random 9)))
			
			;; convert to list index into human numbers
			(setq *listIndex* ( - *humanIndex* 1 ))
			
			;;*val* is the value at the list index position
			(setq *val* (nth *listIndex* board))

			;; my-replace uses human numbers
			(if (string= "-" *val*) (my-replace *humanIndex* "O") ())
			(if (string= "-" *val*) (setq j 100) ())
	)
)

;;AI-MOVEMENT-CONTROL  is for the computer to check to see if any of the spaces in the board are between two X's separated by a space.
(defun AI-MOVEMENT-CONTROL (board)
	;; index variables for the linked list
	(setq v0 (nth 0 board))
	(setq v1 (nth 1 board))
	(setq v2 (nth 2 board))
	(setq v3 (nth 3 board))
	(setq v4 (nth 4 board))
	(setq v5 (nth 5 board))
	(setq v6 (nth 6 board))
	(setq v7 (nth 7 board))
	(setq v8 (nth 8 board))
	;;checks the left-most rows, and columns, and diagonals
	;;checks the left-most spaces in the first row for empty spaces adjacent two to X's
	(if (and (string= v0 "-") (string= v1 "X") (string= v2 "X"))
			(and (print "Replacing 0 with O")(my-replace 1 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v0)))  
		()
	)
	;;checks the left-most spaces in the second row for empty spaces adjacent two to X's
	(if (and (string= v3 "-") (string= v4 "X") (string= v5 "X"))
		(and (my-replace 4 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v3)))
		()
	)
	;;checks the left-most spaces in the third row for empty spaces adjacent two to X's
	(if (and (string= v6 "-") (string= v7 "X") (string= v8 "X"))
		(and (my-replace 7 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v6)))
		()
	)
	;;checks the left-most spaces in the first column for empty spaces adjacent to two X's
	(if (and (string= v0 "-") (string= v3 "X") (string= v6 "X"))
		(and (my-replace 1 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v0)))
		()
	)
	;;checks the left-most spaces in the second column for empty spaces adjacent to two X's
	(if (and (string= v1 "-") (string= v4 "X") (string= v7 "X"))
		(and (my-replace 2 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v1)))
		()
	)
	;;checks the left-most spaces in the third column for empty spaces adjacent to two X's
	(if (and (string= v2 "-") (string= v5 "X") (string= v8 "X"))
		(and (my-replace 3 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v2)))
		()
	)
	;;checks the left-most spaces in the first diagonal for empty spaces adjacent to two X's
	;;check to see if the right diagonal is available
	(if (and (string= v0 "-") (string= v4 "X") (string= v8 "X"))
		(and (my-replace 1 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v0)))
		()
	)
	;;checks the left-most spaces in the last column for empty spaces adjacent to two X's
	(if (and (string= v2 "-") (string= v4 "X") (string= v6 "X"))
		(and (my-replace 3 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v2)))
		()
	)
	;;checks the right-most rows, and columns, and diagonals
	;;checks the right-most spaces in the first row for empty spaces adjacent to two X's
	(if (and (string= v0 "X") (string= v1 "X") (string= v2 "-"))
			(and (my-replace 3 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v2)))  
		()
	)
	;;checks the right-most spaces in the second row for empty spaces adjacent to two X's
	(if (and (string= v3 "X") (string= v4 "X") (string= v5 "-"))
		(and (my-replace 6 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v5)))
		()
	)
	;;checks the right-most spaces in the third row for empty spaces adjacent to two X's
	(if (and (string= v6 "X") (string= v7 "X") (string= v8 "-"))
		(and (my-replace 9 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v8)))
		()
	)
	;;checks the right-most spaces in the first column for empty spaces adjacent to two X's
	(if (and (string= v0 "X") (string= v3 "X") (string= v6 "-"))
		(and (my-replace 7 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v6)))
		()
	)
	;;checks the right-most spaces in the second column for empty spaces adjacent to two X's
	(if (and (string= v1 "X") (string= v4 "X") (string= v7 "-"))
		(and (my-replace 8 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v7)))
		()
	)
	;;checks the right-most spaces in the third column for empty spaces adjacent to two X's
	(if (and (string= v2 "X") (string= v5 "X") (string= v8 "-"))
		(and (my-replace 9 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v8)))
		()
	)
	;;checks the right-most spaces in the diagonal for empty spaces adjacent to two X's
	(if (and (string= v0 "X") (string= v4 "X") (string= v8 "-"))
		(and (my-replace 9 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v8)))
		()
	)
	;;checks the right-most spaces in the diagonal for empty spaces adjacent to two X's
	(if (and (string= v2 "X") (string= v4 "X") (string= v6 "-"))
		(and (my-replace 7 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v6)))
		()
	)
	;;checks the center rows, and columns, and diagonals
	;;checks the right-most spaces in the first row for empty spaces adjacent to two X's
	(if (and (string= v0 "X") (string= v1 "-") (string= v2 "X"))
		(and (my-replace 2 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v1)))  
	()
	)
	;;checks the right-most spaces in the second row for empty spaces adjacent to two X's
	(if (and (string= v3 "X") (string= v4 "-") (string= v5 "X"))
		(and (my-replace 5 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v4)))
		()
	)
	;;checks the right-most spaces in the third row for empty spaces adjacent to two X's
	(if (and (string= v6 "X") (string= v7 "-") (string= v8 "X"))
		(and (my-replace 8 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v7)))
		()
	)
	;;checks the right-most spaces in the first column for empty spaces adjacent to two X's
	(if (and (string= v0 "X") (string= v3 "-") (string= v6 "X"))
		(and (my-replace 4 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v3)))
		()
	)
	;;checks the right-most spaces in the second column for empty spaces adjacent to two X's
	(if (and (string= v1 "X") (string= v4 "-") (string= v7 "X"))
		(and (my-replace 5 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v4)))
		()
	)
	;;checks the right-most spaces in the third column for empty spaces adjacent to two X's
	(if (and (string= v2 "X") (string= v5 "-") (string= v8 "X"))
		(and (my-replace 6 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v5)))
		()
	)
	;;check to see if the center diagonal is available
	(if (and (string= v0 "X") (string= v4 "-") (string= v8 "X"))
		(and (my-replace 5 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v4)))
		()
	)
	;;check to see if the center diagonal is available
	(if (and (string= v2 "X") (string= v4 "-") (string= v6 "X"))
		(and (my-replace 5 "O") (return-from AI-MOVEMENT-CONTROL '(t "the computer placed an" "O" "at location" v4)))
		()
	)
	;;call make-move if all of the if conditionals are false, this makes a normal move instead of a strategic move.
	(make-move *startingBoard*)
	)

;;coin-flipper is a function that uses a pseudo-random number generator
;;to determine who will make the first move in a game.
(defun coin-flipper ()	
	;; set k to be a random integer between 0 and 1
	(setq k (random 100))
	;;if k > 50, the user goes first.
	(if (> k 50) (and  (print k) (print "the User goes first")) ())
	;;if k < 50, the computer goes first.
	(if (<= k 50) (and (print k) (print "the computer goes first") (AI-MOVEMENT-CONTROL *startingBoard*) )())
	;;return the value of k from coin-flipper.
	(return-from coin-flipper k)
)

;;checks to see if there's a cats-game
(defun check-for-cats-game ()
;; run through the list of the board and see if there are any spaces left
    (setq i2 0)
	(dolist (cCheck *startingBoard*)
		( if ( eq i2 9) ( return-from check-for-cats-game t) ()) 
		
		;; if the is a - then there are still spaces, return false (nil)
		(if (string= cCheck "-") (return-from check-for-cats-game nil) ())
		(setq i2 (+ i2 1))
	)
	;; if the are no - then this is a cats game
	(return-from check-for-cats-game t)
)

;;victory, but better, uses a direct comparison method rather than a logical and
(defun check-for-victory (board)
	
	;;setting the variables to be equal to index values.
	;;these act as indexes for each slot in the list.
	;;v0,v1,v2,v3,v4,v5,v6,v7,v8 are all values at positions 0 - 8 on the board
	(setq v0 (nth 0 board))
	(setq v1 (nth 1 board))
	(setq v2 (nth 2 board))
	(setq v3 (nth 3 board))
	(setq v4 (nth 4 board))
	(setq v5 (nth 5 board))
	(setq v6 (nth 6 board))
	(setq v7 (nth 7 board))
	(setq v8 (nth 8 board))
	
	;;the following conditional statements check for the following placements:
	;;X: three in a row horizontally
	;;X: three in a row Vertically
	;;X: three in a row Diagonally
	;;O: three in a row horizontally
	;;O: three in a row Vertically
	;;O: three in a row Diagonally
	
	;; checking the board for horizontal wins for X
	;;each if conditional returns a list containing: 
	;;a boolean value
	;;who won the game
	;;how the win was achieved
	(if (and (string= v0 "X") (string= v1 "X") (string= v2 "X"))
		(return-from check-for-victory '(t "X" "TOP ROW WIN FOR X"))
		()
	)
	(if (and (string= v3 "X") (string= v4 "X") (string= v5 "X"))
		(return-from check-for-victory '(t "X" "MIDDLE ROW WIN FOR X"))
		()
	)
	(if (and (string= v6 "X") (string= v7 "X") (string= v8 "X"))
		(return-from check-for-victory '(t "X" "BOTTOM ROW WIN FOR X"))
		()
	)
	;;checking the board for vertical wins for X
	(if (and (string= v0 "X") (string= v3 "X") (string= v6 "X"))
		(return-from check-for-victory '(t "X" "LEFT COlUMN WIN X"))
		()
	)
	(if (and (string= v1 "X") (string= v4 "X") (string= v7 "X"))
		(return-from check-for-victory '(t "X" "CENTER COlUMN WIN FOR X"))
		()
	)
	(if (and (string= v2 "X") (string= v5 "X") (string= v8 "X"))
		(return-from check-for-victory '(t "X" "RIGHT COlUMN WIN FOR X"))
		()
	)
	;;checking the board for diagonal for X
	(if (and (string= v0 "X") (string= v4 "X") (string= v8 "X"))
		(return-from check-for-victory '(t "X" "DIAGONAL WIN FOR X"))
		()
	)
	(if (and (string= v2 "X") (string= v4 "X") (string= v6 "X"))
		(return-from check-for-victory '(t "X" "DIAGONAL WIN FOR X"))
		()
	)
	;; checking the board for horizontal wins for O
	(if (and (string= v0 "O") (string= v1 "O") (string= v2 "O"))
		(return-from check-for-victory '(t "O" "TOP ROW ACROSS WIN FOR O"))
		()
	)
	(if (and (string= v3 "O") (string= v4 "O") (string= v5 "O"))
		(return-from check-for-victory '(t "O" "MIDDLE ROW ACROSS WIN FOR O"))
		()
	)
	(if (and (string= v6 "O") (string= v7 "O") (string= v8 "O"))
		(return-from check-for-victory '(t "O" "BOTTOM ROW ACROSS WIN FOR O"))
		()
	)
	;;checking the board for vertical wins for O
	(if (and (string= v0 "O") (string= v3 "O") (string= v6 "O"))
		(return-from check-for-victory '(t "O" "LEFT COlUMN WIN FOR O"))
		()
	)
	(if (and (string= v1 "O") (string= v4 "O") (string= v7 "O"))
		(return-from check-for-victory '(t "O" "CENTER COlUMN WIN FOR O"))
		()
	)
	(if (and (string= v2 "O") (string= v5 "O") (string= v8 "O"))
		(return-from check-for-victory '(t "O" "RIGHT COlUMN WIN FOR O"))
		()
	)
	;;checking the board for diagonal for O
	(if (and (string= v0 "O") (string= v4 "O") (string= v8 "O"))
		(return-from check-for-victory '(t "O" "DIAGONAL WIN FOR O"))
		()
	)
	(if (and (string= v2 "O") (string= v4 "O") (string= v6 "O"))
		(return-from check-for-victory '(t "O" "DIAGONAL WIN FOR O"))
		()
	)
	;; return false if it is a cats-game
	(return-from check-for-victory '(nil null "THIS GAME HAS NO WINNER"))
)

;;translates the result of a game into words
(defun list-translator (myMove)
	
	;;detecting who has won the game and how that player won.
	(setq hasWon (nth 0 myMove))
	(setq whoWon (nth 1 myMove))
	(setq winExplain (nth 2 myMove))
	
	;;if a player has won: 
	;;print who won, 
	;;print the win  type
	;;explain how the player won
	;;print out a message to the user to signify that the game is over
	(if (eq t hasWon) (print "THE GAME HAS BEEN WON BY") (return-from list-translator nil))
	
	(print whoWon)
	(print "Win Type: ")
	(print winExplain)	
)

(defun play () 
	;;offset the input when the game is played so that the input values start at 1
	;;then offset the input back to zero so that no index values are skipped.
	(setq *nthValue* ( - index 1))
	
	;;putting an "X", "O", where a "-" in the nth position of the board
	(setq tempStorage (nth *nthValue* *startingBoard*))
	
	;;calls my-replace to change the - to an X
	( if (string= "-" tempStorage) (my-replace index myMark) )
	
	;; call make-move to change the - to an O
	( if (string= "-" tempStorage) ( AI-MOVEMENT-CONTROL *startingBoard*) )
	
	;;print the updated board
	(print "-----------------------")
	(print-board *startingBoard*)
	(print "-----------------------")
	
	;;check for a victory condition.
	(setq *moveResult* (check-for-victory *startingBoard*))
		
	;;list-translator takes in the result form check-for-victory and print it out to the user in a readable message.
	(list-translator *moveResult*)
	;;return the value in a
	(return-from play *moveResult*)
)
;;defining a pseudo main, similar to Java or C.
;;main is a void function, but can have return statements.
(defun main ()

	;;Printing out a header.
	(print "John David Moffa")
	(print "ttt.lisp")
	(print "28 November 2018")
	(print "CSC 450")
	(print "This is a TIC-TAC-TOE game written in LISP")
	
	;; computer goes first if the result from coin-flipper is larger than 50
	;; user goes first is if the result from coin-flipper is less than 50.
	;;call coin-flipper at the beginning of each match.
	(print (coin-flipper))
	;;print the initial starting board using a string list of dashes (-) to denote a spot that hasn't been taken
	(print "-----------------------")
	(print-board *startingBoard*)
	(print "-----------------------")
	
	;;This program dynamically changes the board as you make a move using a linked list.
	;;Previous moves are displayed as part of the game history.

	(defvar index)
	(defvar doMove)
	(setq doMove 0)
	(defvar tempStorage)
	
	;;Offsetting the index so that user input that start at 1 
	;;can access the zeroth position in the list.
	(setq index -1)
	
	;; main loop of the program
	;; keep looping until user types a zero and triggers an exit condition.
	(do ((i 0 (+ i 1)) )
		((= index 0) 'done)
		(print "---------------")
		(print "Enter a number between 1 and 9. (0 ends)")
		(print "1|2|3")
		(print "4|5|6")
		(print "7|8|9")
		;;reading in input from the user via the keypad or numeric keys above the keyboard
		(setq index (read))
		
		;;stops program if the user enters a zero.
		( if ( = index 0) ( return-from main ) () )
		
		;; if the user input is in the right range than play the game
		( if ( and ( > index 0 ) ( < index 10))
			( setq myIterator (play) )(print "Enter a value from 1 to 9")) 
		
		;;reset the game if there is a winner
		(if (eq (nth 0 myIterator) t) 
			(setq *startingBoard* (list "-" "-" "-" "-" "-" "-" "-" "-" "-"))
		())
		(if (eq (nth 0 myIterator) t)
			(print "This game was a win")
		())
		;;call coin-flipper to see if the user goes first or the computer goes first after each round
		(if (eq (nth 0 myIterator) t)
			(coin-flipper)
		())
		;;check-for-cats-game 
		(setq myBool (check-for-cats-game))
		;;visually reset the board after a cats-game
		(if (eq myBool t)
			(setq *startingBoard* (list "-" "-" "-" "-" "-" "-" "-" "-" "-")) 
			()
		)
		(if (eq myBool t)
			(print "This game is a cats-game") 
			()
		)
		;;call coin-flipper after a cats-game
		(if (eq myBool t)
			(coin-flipper) 
			()
		)
		;;visually reset the board.
		
		;; if there is a win
		(if (eq (nth 0 myIterator) t) (print-board *startingBoard*) ())
		;; if there is a cats game
		(if (eq myBool t)(print-board *startingBoard*) ())
	)
)

;; calling main to run the program
(main)